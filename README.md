<!--
SPDX-FileCopyrightText: 2024 gondolyr <gondolyr+code@posteo.org>

SPDX-License-Identifier: CC0-1.0
-->

# Mahjong Assets

This project contains the source game assets for the [Jikaze] game.
This includes things such as the textures, Blender project files, and more.

## Links

- Repository: <https://codeberg.org/amenoasa/mahjong_assets>

# Project Structure

| Directory                     | Description                                                                     |
|-------------------------------|---------------------------------------------------------------------------------|
| `production/`                 | This is where the main production occurs to edit project files.                 |
| `production/3d/`              | This contains anything 3D-related such as Blender source files.                 |
| `production/3d/assets/`       | This contains the static assets to be linked to and used such as model exports. |
| `production/3d/edit/`         | This contains the Blender source files for editing.                             |
| `production/textures/assets/` | This contains the static assets to be used as model textures.                   |
| `production/textures/edit/`   | This contains the texture files that are to be edited.                          |

# Assets Preview

## 3D

| Asset                        | Source Link                                                                                                                              | Preview                                                            |
|------------------------------|------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------|
| `dice_standard_12mm`         | [`production/3d/edit/dice_standard_12mm.blend`](production/3d/edit/dice_standard_12mm.blend)                                             | ![six-sided 12mm die](docs/images/dice_standard_12mm.png)  |
| `junk_mat`                   | [`production/3d/edit/table/junk_mat.blend`](production/3d/edit/table/junk_mat.blend)                                                     | (Colors/materials not final) ![junk mat](docs/images/junk_mat.png) |
| `riichi_tile_default`        | [`production/3d/edit/tiles/riichi_default/riichi_tile_default.blend`](production/3d/edit/tiles/riichi_default/riichi_tile_default.blend) | ![tile models](docs/images/tile_models.png)                        |
| `tenbou`                     | [`production/3d/edit/tenbou/default/tenbou_default.blend`](production/3d/edit/tenbou/default/tenbou_default.blend)                       | ![tenbou models](docs/images/tenbou_models_default.png)            |

## Textures

| Asset          | Source Link                                                                                | Preview                                                              |
|----------------|--------------------------------------------------------------------------------------------|----------------------------------------------------------------------|
| `default` tile | [`production/textures/edit/tiles/default.svg`](production/textures/edit/tiles/default.svg) | ![all riichi mahjong tile faces](docs/images/tiles_inkscape_all.png) |
| `tenbou`       | [`production/textures/edit/tenbou/tenbou.svg`](production/textures/edit/tenbou/tenbou.svg) | ![Tenbou (Point Sticks)](docs/images/tenbou_inkscape.png)            |

## Software

This project uses [free software](https://www.gnu.org/philosophy/free-sw.html) to develop these assets.

Below is a list of software that is used.

- [Blender](https://www.blender.org)
    - `.blend`
- [Inkscape](https://inkscape.org)
    - `.svg`

# License

<a rel="license" href="https://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="https://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.

[Jikaze]: https://codeberg.org/amenoasa/jikaze
